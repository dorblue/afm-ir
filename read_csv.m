function [data] = read_csv (fileName, rowOffset, colOffset)

if (exist("fileName", "var"))
    if (exist("rowOffset", "var") && (exist("colOffset", "var")))
        data = csvread(fileName, rowOffset, colOffset);
    else
        data = csvread(fileName);
    end
else
    error ("File name not provided");
end
    

