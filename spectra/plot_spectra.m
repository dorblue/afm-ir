function [] = plot_spectra (x, y, plotTitle, legendLabels, isPrint)

[numbOfSpectra, ~] = size(y);

uniqueLegendLabels = unique(legendLabels, 'stable');
numbOfUniqueLegendLabels = length(uniqueLegendLabels);
colorMap = colormap(hsv(numbOfUniqueLegendLabels));

j = 1;
groupArr = gobjects(numbOfUniqueLegendLabels, 1);
for i = 1: numbOfUniqueLegendLabels
    currLabel = uniqueLegendLabels(i);
    
    while (legendLabels(j) == currLabel)
        newPlot = plot(x, y(j, :), 'Color', colorMap(i, :));
        
        if (not(isgraphics(groupArr(i))))
            groupArr(i) = newPlot;
        end
        
        hold on;
        j = j + 1;
        if (j > length(legendLabels))
            break;
        end
    end
end

% colorMap = colormap(hsv(numbOfSpectra));

% for i = 1: numbOfSpectra
%     plot(x, y(i, :), 'Color', colorMap(i, :));
%     
%     hold on;
% end

hold off;
set(gca, 'XDir','reverse');

xlabel('Wavenumber [cm^-^1]', 'FontSize', 8);
ylabel('Amplitude [V]', 'FontSize', 8);
axis tight;

title (plotTitle);

if (exist("legendLabels", "var"))
%     legend (legendLabels, 'Location', 'bestoutside');
    legend (groupArr, uniqueLegendLabels, 'Location', 'bestoutside');
end

if ((isPrint == true) && (plotTitle ~= ""))
    print(gcf, strcat(plotTitle, ".tiff"), '-dtiff', '-r600'); 
end