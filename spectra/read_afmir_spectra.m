function [ZData, numbOfPoints] = read_afmir_spectra (data, fileNumber)
          
% - - - There are 6 experimental parameters per each spectrum - - - %            
numbOfParams = 6;
[numbOfWns, numbOfCols] = size(data);
numbOfPoints = numbOfCols / numbOfParams;

wnsArray = zeros(numbOfWns, 1);
spectrMatrix = zeros(numbOfPoints, numbOfWns);
            
for i = 1: numbOfPoints
    % - - - Saving wavenumbers data - - - %
    if (i == 1)
        wnsArray = data(:, 1);
    end
    
    % - - - Saving AFM-IR spectral data - - - %
    spPos = (i - 1) * numbOfParams + 2;
    spectrMatrix(i, :) = data(:, spPos);
end

disp ("BP");

% - - - Creating ZSaisir structure in order to store AFM-IR spectra - - - %
ZData = struct ('d', {}, 'i', {}, 'v', {});

ZData(1).v = wnsArray;
ZData(1).d = spectrMatrix;

ZData(1).i = repmat(horzcat('point ', num2str(fileNumber)), numbOfPoints, 1);
% ZData(1).i = repmat(path(1: length(path) - 1), numbOfPoints, 1);
