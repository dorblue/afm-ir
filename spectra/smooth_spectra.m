function [result] = smooth_spectra(data, method, degree)

[numbOfPoints, numbOfWns] = size(data.d);
dataSm = zeros(numbOfPoints, numbOfWns);

for i = 1: numbOfPoints
    switch method
        case 'sgolay'
            dataSm(i, :) = smooth(data.d(i), method, degree);
        case 'loess'
            dataSm(i, :) = smooth(data.v, data.d(i, :), degree, method);
    end    
end

% figure;
% plot(data.v, data.d);
% set(gca, 'XDir','reverse');
% axis tight;
% 
% hold on;
% plot(data.v, dataSm);
% set(gca, 'XDir','reverse');
% axis tight;
% 
% hold off;

result = struct ('d', {}, 'i', {}, 'v', {});
result(1).d = dataSm;
result(1).v = data.v;
result(1).i = data.i;