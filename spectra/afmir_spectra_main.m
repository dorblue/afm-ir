% - - - Analyzing AFM-IR spectra - - - %

clc;
close all;
clear variables;

% cd 'C:\Users\darya\OneDrive\PhD\Data\AFM-IR spectra_Mucor\YE0P\AFM topography\1600\'
% cd 'C:\Users\darya\OneDrive\PhD\Data\AFM-IR spectra_Mucor\YE0P\Spectral maps\1742\'
% cd 'C:\Users\darya\OneDrive\PhD\Data\AFM-IR spectra_Mucor\YE1P\Spectra\Series\zone 1\'

% - - - Working with AFM-IR spectra (folder by folder) - - - % 
cd 'C:\Users\darya\OneDrive\PhD\Data\AFM-IR spectra_Mucor\YE1P\Spectra\Series\zone 0\'

Akos = genpath('C:\Users\darya\OneDrive\PhD\Codes\Darya\biospec_functions');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\AFM-IR');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\saisir');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\darya_functions');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\darya_functions\plotting');

zoneName = "zone 0";
files = dir('**/*.csv');
folders = string({files.folder}');
numbOfFolders = length(folders);

areaNames = string(zeros(numbOfFolders, 1));
for i = 1: numbOfFolders    
    pathNames = strsplit(folders{i}, '\');
    areaNames(i) = string(pathNames{length(pathNames)});
end

areas = unique(areaNames);
clear areaNames;

% - Here we say that there are 5 replicates per each point - - - %
numbOfRepls = 5;
% numbOfRepls = 3;
numbOfFiles = length(files);
numbOfSpectra = numbOfFiles * numbOfRepls;
numbOfWns = 207; % Find out how to find it without magic numbers!

folderInitNumber = 1;

% - - - Allocating memory to hold AFM-IR spectral values (+ 2nd drv) % - - - 
% - - "ZSpData" is for storing all AFM-IR spectra - - %
ZSpData = struct ('d', {}, 'i', {}, 'v', {});
ZSpData(1).d = zeros(numbOfSpectra, numbOfWns);
ZSpData(1).v = zeros(numbOfWns, 1);
ZSpData(1).i = char(zeros(numbOfSpectra, 8));

% - - "ZSpData_2" is for storing the 2nd derivatives of AFM-IR spectra - - %
ZSpData_2 = ZSpData;

% - - "ZAvData" is for storing averages of AFM-IR spectra - - %
ZAvData = struct ('d', {}, 'i', {}, 'v', {});
ZAvData(1).d = zeros(numbOfFiles, numbOfWns);
ZAvData(1).i = char(zeros(numbOfFiles, 8));
ZAvData(1).v = ZSpData(1).v;

% - - "ZAvData_2" is for storing averages of the 2nd der. of AFM-IR spectra - - %
ZAvData_2 = ZAvData;

zoneIndexes = zeros(numbOfFiles, 1);

fileNumber = 1;
for i = 1: length(areas)
    folderInit = folders{folderInitNumber};
    cd(folderInit);
    fileIndexes = find(folders == string(folderInit));
    % - - - Working with certain area of the spectral image - - - %
    for j = fileIndexes(1): fileIndexes(end)
        zoneIndexes(j) = i;
        % - - - Reading spectral data - - - %
        data = read_csv(files(j).name, 1, 0);
        [ZData, numbOfPoints] = read_afmir_spectra (data, fileNumber);
                
        % - - - Performing analysis of AFM-IR spectra - - - %
        
        % - Calculating 1st and 2nd derivatives - 
        ZData_1 = saisir_derivative(ZData, 2, 9, 1);
        ZData_2 = saisir_derivative(ZData, 2, 9, 2);
        
        % - - - Copying data into ZSpData and ZSpData_2 structs - - - %
        ZSpData.d(((j - 1) * numbOfPoints + 1): (j * numbOfPoints), :) = ZData.d;
        ZSpData_2.d(((j - 1) * numbOfPoints + 1): (j * numbOfPoints), :) = ZData_2.d;
        
        pNameLen = length(ZData.i(1, :));
        ZSpData.i(((j - 1) * numbOfPoints + 1): (j * numbOfPoints), 1: pNameLen) = ZData.i;
        ZSpData_2.i(((j - 1) * numbOfPoints + 1): (j * numbOfPoints), 1: pNameLen) = ZData_2.i;
        
        if (j == 1)
%             wns = flipud(ZData.v);
            wns = ZData.v;
            ZSpData.v = wns;
            ZSpData_2.v = wns;
            ZAvData.v = wns;
            ZAvData_2.v = wns;
        end 
        
        clear data ZData ZData_1 ZData_2; 
  
        % - Having a closer look at the spectra - %
        legendLabels = strcat("Spectrum ", string(1: 5));
        isPrint = false;
        
        % - - Raw spectra - - %
        if (0) 
            plotTitle = strcat("Sample YE0P", " - ", zoneName, ...
                ", point ", string(j));
            figure;
            plot_spectra (ZSpData.v, ...
                ZSpData.d(((j - 1) * numbOfPoints + 1): (j * numbOfPoints), :), ...
                plotTitle, ...
                legendLabels, ...
                isPrint);
        end
        
        % - - Second derivative - - %
        if (0)
            plotTitle = strcat(plotTitle, " (2nd derivative)");
            figure;
            plot_spectra (ZSpData_2.v, ...
                ZSpData_2.d(((j - 1) * numbOfPoints + 1): (j * numbOfPoints), :), ...
                plotTitle, ...
                legendLabels, ...
                isPrint);
            
            close all;
        end
        
        % - - - Averaging AFM-IR data within the area - - - %
        ZAvData.d(fileNumber, :) = mean(ZSpData.d(((j - 1) * numbOfPoints + 1): (j * numbOfPoints), :));
        ZAvData.i(fileNumber, :) = ZSpData.i(j * numbOfPoints, :);
        
        ZAvData_2.d(fileNumber, :) = mean(ZSpData_2.d(((j - 1) * numbOfPoints + 1): (j * numbOfPoints), :));
        ZAvData_2.i(fileNumber, :) = ZSpData_2.i(j * numbOfPoints, :);
        
        fileNumber = fileNumber + 1;
        disp ("BP 1");
    end
    
    folderInitNumber = folderInitNumber + length(fileIndexes);
    
    disp ("BP 0");
end

disp("Random BP");

% - - - Plotting averaged AFM-IR spectra - - - %
figure;
isPrint = true;
plotTitle = strcat("Sample YE1P", " - ", zoneName, " (averaged spectra)");
% legendNames = strcat("Point ", string(1: numbOfFiles));

legendNames = strcat("zone ", string(zoneIndexes));
plot_spectra (ZAvData.v, ZAvData.d, ...
    plotTitle, legendNames, isPrint);

% - - - Plotting averaged 2nd derivative of AFM-IR spectra - - - %
figure;
isPrint = true;
plotTitle = strcat("Sample YE1P", " - ", zoneName, " (averaged 2nd derivative spectra)");
% legendNames = strcat("Point ", string(1: numbOfFiles));

plot_spectra (ZAvData_2.v, ZAvData_2.d, ...
    plotTitle, legendNames, isPrint);

% - - - Performing smoothing of the AFM-IR data - - - %
ZSmData = smooth_spectra(ZAvData, 'loess', 0.08); % can also use 'sgolay'

figure;
plotTitle = "";
plot_spectra (ZSmData.v, ZSmData.d, ...
    plotTitle, legendNames, isPrint);

hold on;
plotTitle = strcat("Sample YE1P", " - ", zoneName, " (averaged + smoothed spectra)");
plot_spectra (ZAvData.v, ZAvData.d, ...
    plotTitle, legendNames, isPrint);
hold off;


% - - - Performing PCA on the collected AFM-IR data - - - %
spRegs = [1500, 1600; ...
          1600, 1700; ...
          1700, 1730;
          1730, 1750;
          1750, 1800];
perf_pca_regs(ZAvData, spRegs, zoneIndexes);

spRegs1 = [1500, 1800];
perf_pca_regs(ZAvData, spRegs1, zoneIndexes);

close all;
disp ("BP FINAL");

