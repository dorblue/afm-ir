% - - - Working with AFM-IR images - - - %

clc;
close all;
clear variables;

% cd 'C:\Users\darya\OneDrive\PhD\Data\AFM-IR spectra_Mucor\YE0P\AFM topography\1600\'
% cd 'C:\Users\darya\OneDrive\PhD\Data\AFM-IR spectra_Mucor\YE0P\Spectral maps\1742\'
cd 'C:\Users\darya\OneDrive\PhD\Data\AFM-IR spectra_Mucor_zones\YE0P\'

Akos = genpath('C:\Users\darya\OneDrive\PhD\Codes\Darya\biospec_functions');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\AFM-IR');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\saisir');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\darya_functions');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\darya_functions\plotting');

% paths = 'Zone 0\';
paths = ['Zone 1\'; 'Zone 2\'; 'Zone 3\'; 'Zone 4\'; 'Zone 5\'];

imageNames = ["protein"; "ffa"; "tag"; "topography"];
% plotTitles = ["Protein region (1600 cm^-^1)", ...
%               "FFA region (1708 cm^-^1)", ...
%               "TAG region (1742 cm^-^1)", ...
%               "AFM topography"];
plotTitles = ["Protein region (1600 cm^-^1)", ...
              "FFA region (1708 cm^-^1)", ...
              "TAG region (1742 cm^-^1)"];

files = dir('**/*.csv');
folders = unique(string({files.folder}'));

numbOfZones = length(paths(:, 1));
numbOfImages = length(files) / numbOfZones;

% Determine some conditions %
isNormalized = false;
isNormalizedAfter = false;

for i = 1: numbOfZones
    % Allocating memory for the structure to hold the image data %
    folderInitNumber = i;
    folderInit = folders{folderInitNumber};
    cd(folderInit);

%     zoneImages = struct ('protein', [], ...
%                          'ffa', [], ...
%                          'tag', [], ...
%                          'topography', []);

    zoneImages = struct ('protein', [], ...
                        'ffa', [], ...
                        'tag', []);
    maxImageEntry = 0;
                     
    % Accessing each zone, one by one %    
    for j = 1: numbOfImages
        k = j + (i - 1) * numbOfImages;
        fileName = files(k).name;

        % Reading image data %
        currImage = read_csv(fileName);

        if (isNormalized == true)
            currImageNorm = normalize_data(currImage);
        else
            currImageNorm = currImage;
        end
        clear currImage;
        
        % Determining the maximum data entry in the current image %
        newMaxImageEntry = max(max(currImageNorm));
        if (newMaxImageEntry >= maxImageEntry)
            maxImageEntry = newMaxImageEntry;
        end
        
        zoneImages = setfield(zoneImages, imageNames(j), currImageNorm);     
    end 
    
    % Performing normalization after loading all image data %
    if (isNormalizedAfter == true)
        newZoneImages = normalize_data_new(zoneImages);
    else
        newZoneImages = zoneImages;
    end
    clear zoneImages;  
    
    % Plotting image data %
    fieldNames = fieldnames(newZoneImages);
        
    figure;
    for j = 1: numbOfImages
        fieldName = fieldNames{j};
        % Plotting an all-image graph on the same scale %
        subplot(2, 2, j);
        make_image(getfield(newZoneImages, fieldName), plotTitles (j), maxImageEntry);
    end
    
    mainTitle = strcat("Sample YE0P - ", ... 
        string(paths(i, 1: length(paths(1, :)) - 1)));
    annotation('textbox', [0 0.9 1 0.1], ...
        'String', mainTitle, ...
        'FontSize', 12, ...
        'FontWeight', 'bold', ...
        'EdgeColor', 'none', ...
        'HorizontalAlignment', 'center');
    
    print(gcf, strcat(mainTitle, ".tiff"), '-dtiff', '-r600');

    % Choosing some particular image regions %
    numbOfCuts = 1;
    while (exist("pointPos_1", "var") && exist("pointPos_2", "var")) 
        dataCut = cut_image(newZoneImages, pointPos_1, pointPos_2);
        structNames = fieldnames(dataCut);
        numbOfZones = numel(structNames);
        
        % Choosing the maximum data entry in the regions of interest %
        maxZoneEntry = max(max(dataCut.ffa));
        newMaxZoneEntry = max(max(dataCut.tag));
        if (newMaxZoneEntry >= maxZoneEntry)
            maxZoneEntry = newMaxZoneEntry;
        end
        
        zoneTitles = ["FFA region (1708 cm^-^1)", ...
                      "TAG region (1742 cm^-^1)"];
                  
        dataCutNorm = dataCut;
        
        figure;
        for k = 1: numbOfZones
            subplot(1, numbOfZones, k);
            currImage = getfield(dataCutNorm, structNames{k});
%             data = normalize_data(data);
            make_image(currImage, zoneTitles(k), maxZoneEntry);
        end
        
        annotation('textbox', [0 0.9 1 0.1], ...
            'String', mainTitle, ...
            'FontSize', 14, ...
            'FontWeight', 'bold', ...
            'EdgeColor', 'none', ...
            'HorizontalAlignment', 'center');
    
        print(gcf, strcat(mainTitle, " (", string(numbOfCuts), ")", ...
            ".tiff"), '-dtiff', '-r600');
        
        numbOfCuts = numbOfCuts + 1;
        clear pointPos_1 pointPos_2;
        disp ("BP");
    end
    
%     print(gcf, strcat(mainTitle, ".tiff"), '-dtiff', '-r600');
    
    % - - - Plotting height profiles - - - %
    while (exist("cursor_info", "var"))
        describe_height_profiles (zoneImages, numbOfImages, ... 
                                  imageNames, cursor_info, paths (i, 1:6));
        clear cursor_info;
%         for j = 1: numbOfImages
%             % - - - Plotting in horizontal direction - - - %
%             make_height_profile(getfield(zoneImages, char(zoneNames(j))), cursor_info, "x");
%             % - - - Plotting in vertical direction - - - %
%             make_height_profile(dataNorm, cursor_info, "y");
%             clear cursor_info;
% 
%             disp ("BP 2");
%         end
    end
    
    close all;
%     make_3d_image (dataNorm);
end

close all;
disp ("BP 0");