function [] = describe_height_profiles (zoneImages, numbOfFiles, ... 
                    imageNames, cursorInfo, zoneName)

plotTitles = ["1600 cm-1", ...
    "1708 cm-1", ...
    "1742 cm-1", ...
    "AFM topography"];

figure;

% - - - Plotting AFM topography (height) profile - - - %
subplot(3, 1, 1);
make_height_profile (getfield(zoneImages, char(imageNames(4))), ...
        cursorInfo, "x");
legend (plotTitles(4));
title (strcat("Height profile - ", zoneName));
    
% - - - Plotting absorption intensity profiles - - - %
subplot(3, 1, 2);

for i = 1: (numbOfFiles - 1)
    
    make_height_profile (getfield(zoneImages, char(imageNames(i))), ...
        cursorInfo, "x");
    hold on;
end

legend (plotTitles(1: (numbOfFiles - 1)));
title (strcat("Absorption intensity profiles - ", zoneName));
hold off;

% - - - Plotting height/absorption ratio profiles - - - %
subplot(3, 1, 3);
hgtVsAbsRatio = {[], [], []};

for i = 1: (numbOfFiles - 1)
    
    hgtVsAbsRatio{i} = getfield(zoneImages, char(imageNames(i)))./ ...
                        getfield(zoneImages, char(imageNames(4)));
    
    make_height_profile (hgtVsAbsRatio{i}, cursorInfo, "x");
    hold on;
end

legend (plotTitles(1: (numbOfFiles - 1)));
title (strcat("Height/absorption intensity ratio profiles - ", zoneName));
hold off;

% - - - Plotting height/absorption ratio images - - - %
figure;
for i = 1: (numbOfFiles - 1)
    subplot(1, 3, i);
    make_image(hgtVsAbsRatio{i}, plotTitles (i));
end

disp ("Done!");