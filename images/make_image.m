function [] = make_image (data, plotTitle, dataMax)

% figure;

image (data, 'CDataMapping', 'scaled');
caxis([0 dataMax]);
colormap (jet);
axis normal;
% caxis('auto');
% caxis(img, [0 2]);
colorbar;

t = title (plotTitle);
t.FontSize = 12;
t.FontWeight = 'normal';