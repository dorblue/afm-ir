function [dataCut] = cut_image (data, pointPos_1, pointPos_2)

dataCut = struct('ffa', data.ffa, ...
                 'tag', data.tag);

x1 = pointPos_1.Position(1);
y1 = pointPos_1.Position(2);

x2 = pointPos_2.Position(1);
y2 = pointPos_2.Position(2);

% pntsPos.x11 = pntsPos.x2;
% pntsPos.y11 = pntsPos.y1;
% pntsPos.x21 = pntsPos.x1;
% pntsPos.y21 = pntsPos.y2;

if (y1 > y2)
    dataCut.ffa = dataCut.ffa(y2: y1, :);
    dataCut.tag = dataCut.tag(y2: y1, :);
else
    dataCut.ffa = dataCut.ffa(y1: y2, :);
    dataCut.tag = dataCut.tag(y1: y2, :);
end

if (x1 > x2)
    dataCut.ffa = dataCut.ffa(:, x2: x1);
    dataCut.tag = dataCut.tag(:, x2: x1);
else
    dataCut.ffa = dataCut.ffa(:, x1: x2);
    dataCut.tag = dataCut.tag(:, x1: x2);
end
