function [] = make_3d_image (data)

[rowsNumber, colsNumber] = size (data);

x = 1: rowsNumber;
y = 1: colsNumber;

figure;

surf (x, y, data);
colormap jet;
colorbar;

disp ("End of story!");
