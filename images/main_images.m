% - - - Working with AFM-IR images - - - %

clc;
close all;
clear variables;

% cd 'C:\Users\darya\OneDrive\PhD\Data\AFM-IR spectra_Mucor\YE0P\AFM topography\1600\'
cd 'C:\Users\darya\OneDrive\PhD\Data\AFM-IR spectra_Mucor\YE0P\Spectral maps\1742\'

files = dir('**/*.csv');
filesNumber = length(files);

for i = 1: filesNumber
    data = read_csv(files(i).name); 
    dataNorm = normalize_data(data);
    
    % - - - Data -> image + 3d image - - - 
    make_image(dataNorm);
    disp ("BP 1");
    
    % - - - Plotting height profiles - - - %
    while (exist("cursor_info", "var"))
        % - - - Plotting in horizontal direction - - - %
        make_height_profile(dataNorm, cursor_info, "x")
        % - - - Plotting in vertical direction - - - %
        make_height_profile(dataNorm, cursor_info, "y")
        clear cursor_info
        
        disp ("BP 2");
    end
    
    make_3d_image (dataNorm);
end

disp ("BP 0");