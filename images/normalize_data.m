function [dataCorr] = normalize_data (data)

minLim = 0;
maxLim = 1;

[rowsNumber, colsNumber] = size(data);
dataCorr = zeros(rowsNumber, colsNumber);

dataMin = min(min(data));
dataMax = max(max(data));
    
for i = 1: colsNumber
%     dataMin = min(data(:, i));
%     dataMax = max(data(:, i));
    dataCorr(:, i) = minLim + (maxLim - minLim)*((data(:, i) - dataMin)/(dataMax - dataMin));
    disp ("Crap");
end

disp ("End of story!");