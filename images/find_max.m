function [maxData] = find_max (data)

maxData = 0;

newMaxData = max(max(data));
if (newMaxData >= maxData)
    maxData = newMaxData;
end