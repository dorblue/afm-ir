function [] = make_height_profile (data, cursorInfo, direction, imageName)

% [rows, cols] = size(cursorInfo.Target.CData);
[rows, cols] = size(data);
cursorPos = cursorInfo.Position;

% figure;
switch direction
    case "x"
        pos = cursorPos(2);
        x = (1: rows);
%         y = cursorInfo.Target.CData(:, pos);
        y = data(pos, :);
        if (exist("imageName", "var"))
            plotTitle = strcat (imageName, " ", "profile (x axis)");
        else
            plotTitle = "";
%             plotTitle = "AFM topography - height profile (x axis)";
        end
        
        xTitle = "x";
    case "y"
        pos = cursorPos(1);
        x = (1: cols);
%         y = cursorInfo.Target.CData(pos, :);
        y = data(:, pos);
        
        if (exist("imageName", "var"))
            plotTitle = strcat (imageName, " ", "profile (y axis)");
        else
            plotTitle = "";
%             plotTitle = "AFM topography - height profile (y axis)";
        end
        
        xTitle = "y";
    otherwise
        % Add something to process errors
end

% - - - Plotting height profile - - -
plot (x, y);
title (plotTitle);
xlabel (xTitle);
ylabel ("Height");