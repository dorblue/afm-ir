function [dataNormalized] = normalize_data_new (data)

newData = [];

fieldNames = fieldnames(data);
numbOfZones = numel(fieldNames);

initMatrixSize = struct('rows', [], 'cols', []);

for i = 1: numbOfZones
    currFieldName = fieldNames{i};
    
    dataInit = getfield(data, currFieldName);
    [nrows, ncols] = size (dataInit);
    
    initMatrixSize.rows = nrows;
    initMatrixSize.cols = ncols;
    
    dataReshaped = reshape (dataInit, [nrows * ncols, 1]);
        
    newData = horzcat(newData, dataReshaped);
end

 % - - - Performing normalization - - - %
minLim = 0;
maxLim = 1;

[rowsNumber, colsNumber] = size(newData);
dataCorr = zeros(rowsNumber, colsNumber);

% dataMin = min(min(newData));
% dataMax = max(max(newData));
    
for i = 1: colsNumber
%     dataCorr(:, i) = minLim + (maxLim - minLim)*((newData(:, i) - dataMin)/(dataMax - dataMin));

    % - - - Centering and scaling - - - %
    dataCorr(:, i) = (newData(:, i) - mean(newData(:, i)))/std(newData(:, i));
    
    % - - - Normalization - - - %
    dataMin = min(dataCorr(:, i));
    dataMax = max(dataCorr(:, i));
    dataCorr(:, i) = minLim + (maxLim - minLim)*((dataCorr(:, i) - dataMin)/(dataMax - dataMin));

    disp ("Crap");
end

% - - - Allocating memory for the structure - - - %
dataNormalized = data;

for i = 1: numbOfZones
    currFieldName = fieldNames{i};    
    dataMatrix = dataCorr(:, i);
    
    dataMatrixReshaped = reshape(dataMatrix, [initMatrixSize.rows, initMatrixSize.cols]);
    dataNormalized = setfield(dataNormalized, ...
        currFieldName, dataMatrixReshaped);
end

disp ("End of story!");